# Net Neutrality MONitor

This django application is used to collect data about net neutrality violation,
to allow a name and shame policy.

# Setup

You have to clone this branch (django-1.8) to get the latest source code.
The recommended way is to use a virtualenv and to run the app through apache and
uwsgi.

## System dependencies

You need to install some development package to activate al the needed python
modules.

On Debian or Ubuntu, this will be done with the following command:

    sudo apt-get install libxml2-dev libxslt-dev build-essential python-dev libsqlite3-dev libjpeg-dev zlib1g-dev libapache2-mod-uwsgi libapache2-mod-wsgi uwsgi-plugin-python libfreetype6-dev uwsgi

## Create a virtualenv

The recommended way is to install a python-virtualenv and to run the django
application from there.

    virtualenv nnmon-env
    source nnmon-env/bin/activate

## Get the source

You need to git clone the repository:

    git clone gitlab@git.laquadrature.net/la-quadrature-du-net/respect-my-net.git respect-my-net

## Install the module dependencies

Now, you need to install all the needed python modules, with pip.

    pip install --upgrade -r nnmon-requirements.txt

## Configure django

Edit the configuration file for django. Pay special attention to DEBUG, various
path and SECRET_KEY settings. Next you need to setup the database, using the
manage.py tool.

    python manage.py migrate

## Run server

To run server, just do:
    python manage.py runserver

# To deploy in production

## Set-up uwsgi

You need to create an application for uwsgi. Create a file in
`/etc/uwsgi/apps-available` with the following content. Adapt to your needs
and read the documentation:

    [uwsgi]
    plugins = python
    uid = www-data

    pythonpath = /home/rmn/respect-my-net
    module = nnmon.wsgi:application
    env = DJANGO_SETTINGS_MODULE=nnmon.settings
    chdir = /home/rmn/respect-my-net
    home = /home/rmn/nnmon-env

    vacuum = True
    master = True
    max-requests = 5000
    pidfile = /run/uwsgi/app/rmn/pid
    daemonize = /var/log/uwsgi/rmn.log
    socket = /run/uwsgi/app/rmn/socket

PythonPath is where the django app is based (ie: the root of the git
repository), chdir is the same path. Home, is the path of the virtualenv created
above.

Now you need to activate the apps by creating a symoblic link into
`/etc/uwsgi/apps-enabled`, and to restart uwsgi.

    cd /etc/uwsgi/apps-enabled
    ln -s ../apps-available/nnmond.conf
    service uwsgi restart

## Set-up apache

You need to setup apache (or nginx depending on what you prefer). First, you
need to actvivate the uwsgi module:

    a2enmod uwsgi
    service apache2 restart

And now you need to create a VirtualHost in
`/etc/apache2/sites-availabe/nnmon.conf`

    <virtualhost *:80>
        ServerName rmn.example.com

        SetHandler uwsgi-handler
        uWSGISocket /run/uwsgi/app/rmn/socket

        LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %T
    %{Host}i" alternc
        CustomLog /var/log/apache2/access.log alternc
        ErrorLog /var/log/apache2/rmn.error.log
    </virtualhost>

You need to activate the website and the to reload apache. Of course, for
production use, you should use a TLS VirtualHost.

    a2ensite nnmon
    service apache2 reload

