Babel
BabelDjango
BeautifulSoup
defusedxml
Django==1.8.6
django-ajax-selects
django-bootstrap3
django-contrib-comments
django-debug-toolbar
django-haystack
django-registration
django-simple-captcha
django-tastypie
django-tinymce
flup
httplib2
lxml
oauth2
odslib
Pillow
pysqlite
python-dateutil
python-mimeparse
python-twitter
pytz
simplejson
six
South
sqlparse
wheel
Whoosh
mysql-python
